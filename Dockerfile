FROM amazoncorretto:11
EXPOSE 9001
ADD target/auth-security-service.jar auth-security-service.jar
ENTRYPOINT ["java","-jar","/auth-security-service.jar"]