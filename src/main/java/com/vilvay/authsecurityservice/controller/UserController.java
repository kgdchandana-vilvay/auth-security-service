package com.vilvay.authsecurityservice.controller;

import com.vilvay.authsecurityservice.dto.UserDto;
import com.vilvay.authsecurityservice.entity.AuthUser;
import com.vilvay.authsecurityservice.service.AuthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oauth/users")
public class UserController {

  @Autowired
  private AuthUserService authUserService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public AuthUser register(@RequestBody UserDto userDto) {
    return authUserService.register(userDto);
  }

}
