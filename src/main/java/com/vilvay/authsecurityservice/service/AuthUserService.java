package com.vilvay.authsecurityservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vilvay.authsecurityservice.dto.UserDto;
import com.vilvay.authsecurityservice.entity.AuthUser;
import com.vilvay.authsecurityservice.repository.UserRepository;
import com.vilvay.authsecurityservice.repository.UserRoleRepository;
import java.util.Collections;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserRoleRepository userRoleRepo;

  @Autowired
  private PasswordEncoder passwordEncoder;

  public AuthUser register(UserDto userDto) {
    AuthUser authUser = new ObjectMapper().convertValue(userDto, AuthUser.class);
    authUser.setPassword(passwordEncoder.encode(userDto.getPassword()));
    authUser.setRoles(Collections.singletonList(userRoleRepo.findByRoleNameContaining("USER")));
    Optional<AuthUser> optUser = userRepository.findByUserNameOrEmail(userDto.getUserName(), userDto.getEmail());
    if (!optUser.isPresent()) {
      return userRepository.save(authUser);
    }
    throw new RuntimeException("User already exist");
  }
}
