package com.vilvay.authsecurityservice.repository;

import com.vilvay.authsecurityservice.entity.AuthUser;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AuthUser, Long> {
  Optional<AuthUser> findByUserName(String username);

  Optional<AuthUser> findByUserNameOrEmail(String username, String email);
}
