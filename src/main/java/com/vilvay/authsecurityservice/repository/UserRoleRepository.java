package com.vilvay.authsecurityservice.repository;

import com.vilvay.authsecurityservice.entity.AuthRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<AuthRole, Long> {
  AuthRole findByRoleNameContaining(String roleName);
}
